package com.martifer.android.composemaps.data.remote.model

import com.martifer.android.composemaps.domain.Place
import com.martifer.android.composemaps.domain.DomainMapper

class PlaceDtoMapper : DomainMapper<PlaceDto, Place> {

    override fun mapToDomainModel(model: PlaceDto): Place {
        return Place(
            name = model.name,
            source = model.source,
            longitude = model.longitude,
            latitude = model.latitude,
            radius = model.radius,
            discovered = false,
            caught = false,
            hidden = true,
        )
    }

    override fun mapFromDomainModel(domainModel: Place): PlaceDto {
        return PlaceDto(
            name = domainModel.name,
            source = domainModel.source,
            longitude = domainModel.longitude,
            latitude = domainModel.latitude,
            radius =  domainModel.radius
        )
    }

    fun fromEntityList(initial: List<PlaceDto>): List<Place>{
        return initial.map { mapToDomainModel(it) }
    }

    fun toEntityList(initial: List<Place>): List<PlaceDto>{
        return initial.map { mapFromDomainModel(it) }
    }


}