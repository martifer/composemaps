package com.martifer.android.composemaps.home

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.martifer.android.composemaps.domain.Place
import com.martifer.android.composemaps.data.PlaceEntity
import com.martifer.android.composemaps.data.RepositoryImpl
import com.martifer.android.composemaps.data.remote.model.PlaceDto
import com.martifer.android.composemaps.data.remote.model.PlaceDtoMapper
import kotlinx.coroutines.*
import retrofit2.Response

class HomeViewModel(private val repository: RepositoryImpl,
                    private val dispatcher: Dispatchers,
                    private val domainMapper: PlaceDtoMapper
)
    : ViewModel() {

   // val liveDataDetail = MutableLiveData<UserEntity>()

    var job: Job? = null

    private val TAG = "HomeVM-allappli"

    val imatges = MutableLiveData<List<PlaceDto>>()
    var firstImatge = MutableLiveData<PlaceDto>()
    //val imatgeLoadError = MutableLiveData<String?>()
    //val loading = MutableLiveData<Boolean>()
    var navegar: Boolean = false
    var indexImatge: Int = 0

    var progress: Int = 0;
    var count: Int =0;
    var loadError = mutableStateOf("")
    var isLoading = mutableStateOf(true)
    var endReached = mutableStateOf(false)

    var permissionCamera = mutableStateOf(false)


    val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        onError("Exception: ${throwable.localizedMessage}")
    }

    init {
        fetchImages()
    }

    fun fetchImages() {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {

            count = getCount()
            Log.v(TAG, "fetchImages, current count: "+ count)
            if(count==0){
                Log.v(TAG, "fetchImages: baixant")
                val response : Response<List<PlaceDto>> = repository.fetchImages()
                withContext(Dispatchers.Main) {
                    if(response.isSuccessful) {
                        Log.v(TAG, "fetchImages: success")
                        imatges.value = response.body()
                        response.body()?.let { domainMapper.fromEntityList(it) }?.let { insertAll(it) }
                        count = imatges.value!!.size
                        progress = getProgress()
                        isLoading.value = false
                    } else {
                        Log.v(TAG, "fetchImages: error")
                        onError("Error:  ${response.message()}")
                        progress = getProgress()
                    }
                }
            } else {
                Log.v(TAG, "fetchImages: no cal baixar imatges: loading value false")


                withContext(Dispatchers.Main) {
                    progress = getProgress()
                    isLoading.value = false
                    Log.v(
                        TAG,
                        "fetchImages: no cal baixar imatges: loading value -->" + isLoading.value
                    )
                }
            }
        }
    }

    private suspend fun insertAll (places: List<Place>) {
        repository.insertAll(convertToPlaceEntityList(places))
    }


    private fun convertToPlaceEntityList(users: List<Place>): List<PlaceEntity> {
        return users.map { PlaceEntity(it) }
    }


    private suspend fun getCount(): Int {
        return repository.getCount()
    }

    private suspend fun getProgress(): Int {
        return repository.getProgress()
    }

    private fun onError(message: String) {
        //loadError.value = message
        isLoading.value = false
    }
}