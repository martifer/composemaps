package com.martifer.android.composemaps.di

import android.app.Application
import android.icu.util.TimeUnit
import androidx.room.Room
import com.martifer.android.composemaps.camera.CameraViewModel
import com.martifer.android.composemaps.data.RepositoryImpl
import com.martifer.android.composemaps.data.local.LocalDataSource
import com.martifer.android.composemaps.data.local.PlacesDao
import com.martifer.android.composemaps.data.local.PlacesDatabase
import com.martifer.android.composemaps.data.remote.ImatgeAPI
import com.martifer.android.composemaps.data.remote.RemoteDataSource
import com.martifer.android.composemaps.data.remote.model.PlaceDtoMapper
import com.martifer.android.composemaps.gallery.GalleryViewModel
import com.martifer.android.composemaps.game.GameViewModel
import com.martifer.android.composemaps.home.HomeViewModel
import com.martifer.android.composemaps.home.TestViewModel
import com.martifer.android.composemaps.ranking.RankingViewModel
import com.martifer.android.composemaps.service.LocationService
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create


val repositoryModule = module {
    single { RepositoryImpl(get(),get(), get(), get()) }
}

val localModule = module {
    single { provideLocalDataSource(get())}
    single { provideDatabase(androidApplication()) }
    single { provideDao(get()) }
}

fun provideLocalDataSource(placesDatabase: PlacesDatabase): LocalDataSource = LocalDataSource(placesDatabase)

fun provideDao(database: PlacesDatabase): PlacesDao {
    return  database.placesDao()
}

fun provideDatabase(application: Application): PlacesDatabase {
    return Room.databaseBuilder(
        application,
        PlacesDatabase::class.java,
        "places_database"
    )
        .build()
}

val remoteModule = module {

    single { provideRemoteDataSource(get()) }
    single { provideRetrofit(get()) }
    single { provideIgdbApiClient(get()) }
    factory { provideLoggingInterceptor() }
    factory { provideOkHttpClient(get()) }
}

fun provideRemoteDataSource(ApiClient: ImatgeAPI): RemoteDataSource = RemoteDataSource(ApiClient)

fun provideIgdbApiClient(retrofit: Retrofit): ImatgeAPI = retrofit.create(ImatgeAPI::class.java, )

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl("https://bitbucket.org/martifer/imatges/")
        .client(okHttpClient)
        .build()
}

fun provideOkHttpClient( loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
    return OkHttpClient.Builder().apply {
        this.addInterceptor(loggingInterceptor)
        this.connectTimeout(2, java.util.concurrent.TimeUnit.MINUTES)
    }.build()
}

fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    return HttpLoggingInterceptor().apply {
        this.level = HttpLoggingInterceptor.Level.BODY
    }
}

val homeViewModelModule = module {
    viewModel {
        HomeViewModel(get(), get(), get())
    }
}

val gameViewModelModule = module {
    viewModel {
        GameViewModel(get(), get())
    }
}

val galleryViewModelModule = module {
    viewModel {
        GalleryViewModel(get())
    }
}

val cameraViewModelModule = module {
    viewModel {
        CameraViewModel(get())
    }
}

val testViewModelModule = module {
    viewModel {
        TestViewModel(get(),get(),get())
    }
}

val rankingViewModelModule = module {
    viewModel {
        RankingViewModel(get(),get(),get())
    }
}




/*module {
    viewModel { MyViewModel() }
}*/


val dispatchersModule = module {
    factory { provideDispatchers() }

}

fun provideDispatchers(): Dispatchers = Dispatchers

val dtoMapperModule = module {
    factory { providePlaceDtoMapper() }

}

fun providePlaceDtoMapper(): PlaceDtoMapper = PlaceDtoMapper()


val serviceModule = module {
    factory { provideLocationService() }

}

fun provideLocationService(): LocationService = LocationService()