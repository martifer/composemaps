package com.martifer.android.composemaps.game

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import coil.compose.rememberImagePainter

@Composable
fun ImageScreen (navController: NavController, viewModel: GameViewModel){

    val TAG = "ImgScr-allappli"

    /*val argument = 123456
    route = screen.route.plus("/{$argument}")
    navController.navigate(route)
*/

        val index = viewModel.imageIndex.value
        Log.v(TAG, "ImageScreen: " + viewModel.listPlaces.size )
        val source = viewModel.listPlaces.get(index).place.source

        Column() {
            Text(text = "Image Screen", color = Color.White)
            Image(
                painter = rememberImagePainter(source),//("https://bitbucket.org/martifer/imatges/raw/master/vallnuria.jpg"),
                contentDescription = null,
                modifier = Modifier
                    .fillMaxSize()
                    .fillMaxWidth()
                    .clickable(
                        enabled = true,
                        onClickLabel = "Clickable image",
                        onClick = {
                            viewModel.imageScreen.value = false
                        //navController.navigate(Screen.GameScreen.route)
                        }
                    )
            )
            Text(text = "Remember this place and click when you are ready", color = Color.White)

        }
}

@Preview
@Composable
fun PreviewImageScreen() {
    val navController = rememberNavController()
    //ImageScreen(navController)
}