package com.martifer.android.composemaps

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.martifer.android.composemaps.permission.LocationPermisionUI
import com.martifer.android.composemaps.service.LocationService
import org.koin.android.ext.android.inject


class MainActivity : ComponentActivity() {

    private val TAG = "mainAct-allappli"

    // inject LocationService into property
    val service: LocationService by inject()

    @ExperimentalPermissionsApi
    @ExperimentalFoundationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //
        //if(PermissionUtils.checkIfPermissionGranted(context = this.applicationContext, Manifest.permission.ACCESS_FINE_LOCATION))
        //  actionOnService() // just for testing service


        setContent {
            LocationPermisionUI {
                //actionOnService()
            }

            Surface(
                color = Color(0xFF101010),
                modifier = Modifier.fillMaxSize()
            ) {
                Navigation { GoogleSignIn() }
            }

            /*val windowSizeClass = rememberWindowSizeClass()
            ComposeMapsApp(windowSizeClass)
            */
            /*ComposemapsTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Greeting("Android")
                }
            }*/
        }
    }


    private fun actionOnService() {

        Intent(this, service.javaClass).also {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Log.v(TAG, "onStartCommand 26 or more")
                startForegroundService(it)
                return
            }
            Log.v(TAG, "onStartCommand 26 or less")
            startService(it)
        }
    }


    //region User Google Sign-in and sign-out Code

    fun getGoogleSinginClient(): GoogleSignInClient {
        /**
         * Configure sign-in to request the user's ID, email address, and basic
         * profile. ID and basic profile are included in DEFAULT_SIGN_IN.
         */
        val gso = GoogleSignInOptions
            .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .requestProfile()
            .build()


        /**
         * Build a GoogleSignInClient with the options specified by gso.
         */
        return GoogleSignIn.getClient(this, gso)
    }

    fun GoogleSignIn() {

        if (!isUserSignedIn()) {

            val signInIntent = getGoogleSinginClient().signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        } else {
            Toast.makeText(this@MainActivity, " User already signed-in ", Toast.LENGTH_SHORT).show()
        }

    }

    private fun isUserSignedIn(): Boolean {

        val account = GoogleSignIn.getLastSignedInAccount(this)
        return account != null

    }

    private fun signout() {
        if (isUserSignedIn()) {
            getGoogleSinginClient().signOut().addOnCompleteListener {
                if (it.isSuccessful) {
                    Toast.makeText(this@MainActivity, " Signed out ", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this@MainActivity, " Error ", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {

            handleSignData(data)

        }
    }

    private fun handleSignData(data: Intent?) {
        // The Task returned from this call is always completed, no need to attach
        // a listener.
        GoogleSignIn.getSignedInAccountFromIntent(data)
            .addOnCompleteListener {
                "isSuccessful ${it.isSuccessful}"//.print()
                if (it.isSuccessful) {
                    // user successfully logged-in
                    Log.v(TAG, "account ${it.result?.account}")
                    Log.v(TAG, "displayName ${it.result?.displayName}")
                    Log.v(TAG, "Email ${it.result?.email}")
                } else {
                    // authentication failed
                    //Log.v(TAG, it.exception.message.toString())
                    "exception ${it.exception}"//.print()
                }
            }

    }

    //endregion


    companion object {
        const val RC_SIGN_IN = 0
        const val TAG_KOTLIN = "TAG_KOTLIN"
    }

}