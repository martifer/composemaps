package com.martifer.android.composemaps.game

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import org.koin.androidx.compose.getViewModel

@Composable
fun GameMainScreen(navController: NavController) {

    val viewModel = getViewModel<GameViewModel>()
    if (viewModel.isGameReady.value) {
        if (viewModel.imageScreen.value) {
            ImageScreen(navController = navController, viewModel = viewModel)
        } else {
            GameScreen(navController = navController, viewModel = viewModel)
        }
    }
}