package com.martifer.android.composemaps.service

import android.R
import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.os.Build
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY
import com.google.android.gms.location.LocationResult
import com.martifer.android.composemaps.data.PlaceEntity
import com.martifer.android.composemaps.data.RepositoryImpl
import com.martifer.android.composemaps.other.ServiceUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

// https://stackoverflow.com/questions/55122842/using-lateinit-properties-in-constructor-in-kotlin
class LocationService : LifecycleService() {

    val repository: RepositoryImpl by inject()

    private val TAG = "LocSer-allappli"

    lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    lateinit var listPlaces: List<PlaceEntity>

    companion object {
        val isTracking = MutableLiveData<Boolean>()
    }

    private fun postInitialValues() {
        isTracking.postValue(false)
    }

    fun prepareList() {
        CoroutineScope(Dispatchers.IO).launch {
            val placesList = repository.getNonDiscoveredPlaces()
            Log.v(TAG, "prepareGame, places found:" + placesList.size)
            listPlaces = placesList
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        Log.v(TAG, "onStartCommand")
        return START_STICKY
    }

    override fun onCreate() {
        Log.v(TAG, "onCreate")
        super.onCreate()
        postInitialValues()
        prepareList()
        fusedLocationProviderClient = FusedLocationProviderClient(this)

        showToast(context = applicationContext, "onCreate service")

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) createNotificationChanel() else startForeground(
            1,
            Notification()
        )

        updateLocation()

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChanel() {
        //Log.v(TAG, "createNotificationChanel")
        val NOTIFICATION_CHANNEL_ID = "com.getlocationbackground"
        val channelName = "Background Service"
        val chan = NotificationChannel(
            NOTIFICATION_CHANNEL_ID,
            channelName,
            NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val manager =
            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
        manager.createNotificationChannel(chan)
        val notificationBuilder =
            NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
        val notification: Notification = notificationBuilder.setOngoing(true)
            .setContentTitle("location service running")
            .setPriority(NotificationManager.IMPORTANCE_MAX)
            .setCategory(Notification.CATEGORY_SERVICE)
            .setSmallIcon(R.drawable.ic_menu_myplaces)  // sense aixo no es veu el títol
            .build()
        //showToast(context = applicationContext,"createNotificationChanel" )
        startForeground(2, notification)
    }

    fun showToast(context: Context, text: String) {
        val duration = Toast.LENGTH_LONG
        val toast = Toast.makeText(context, text, duration)
        toast.show()
    }

    val locationCallback = object : LocationCallback() {
        override fun onLocationResult(result: LocationResult?) {
            Log.v(TAG, "onLocationResult")
            super.onLocationResult(result)
            result?.locations?.let { locations ->
                var refreshList = false
                for (location in locations) {
                    for (place in listPlaces) {
                        val results = FloatArray(1)
                        Location.distanceBetween(
                            place.place.latitude,
                            place.place.longitude,
                            location.latitude,
                            location.longitude,
                            results
                        )

                        Log.v(TAG, "checkResult: Distance: " + results[0])
                        if (results[0] < place.place.radius * 2) {
                            refreshList = true
                            //showToast(applicationContext, "place found")
                            CoroutineScope(Dispatchers.IO).launch {
                                repository.updateCaught(true, place.id) // update caught
                                Log.v(TAG, "one place caught:" + place.place.name)
                                //showNotification()
                            }
                        }
                    }
                }
                if (refreshList) {
                    prepareList()
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun updateLocation() {
        if (ServiceUtils.hasLocationPermissions(this)) {
            val request = LocationRequest().apply {
                interval = 2000L
                fastestInterval = 5000L
                priority = PRIORITY_HIGH_ACCURACY
            }
            fusedLocationProviderClient.requestLocationUpdates(
                request,
                locationCallback,
                Looper.getMainLooper()
            )
        } else {
            Log.v(TAG, "no permission provided")
        }
    }
}