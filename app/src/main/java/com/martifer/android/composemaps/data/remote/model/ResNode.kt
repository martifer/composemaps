package com.martifer.android.composemaps.data.remote.model

data class ResNode(
    val status: String,
    val msg: String
)