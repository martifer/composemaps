package com.martifer.android.composemaps.data


import com.martifer.android.composemaps.data.local.LocalDataSource
import com.martifer.android.composemaps.data.local.PlacesDao
import com.martifer.android.composemaps.data.remote.RemoteDataSource
import com.martifer.android.composemaps.data.remote.model.PlaceDto
import com.martifer.android.composemaps.data.remote.model.PlaceDtoMapper
import com.martifer.android.composemaps.data.remote.model.ResNode
import com.martifer.android.composemaps.domain.DomainMapper
import com.martifer.android.composemaps.domain.Place
import com.martifer.android.composemaps.domain.Repository
import com.martifer.android.composemaps.domain.Score
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import java.io.InputStream

class RepositoryImpl(private val placesDao: PlacesDao,
                     private val localDataSource: LocalDataSource,
                     private val remoteDataSource: RemoteDataSource,
                     private val domainMapper: PlaceDtoMapper,
): Repository {
    override suspend fun getProgress(): Int {
        return localDataSource.getProgress()
    }

    override suspend fun getCount(): Int {
        return localDataSource.getCount()
    }

    override suspend fun fetchImages(): Response<List<PlaceDto>> {
        return remoteDataSource.getImages() //.body()?.let { domainMapper.fromEntityList(it) }
    }

    override suspend fun postImage(inputStream: InputStream): Response<ResNode>{
        return remoteDataSource.postImage(inputStream)
    }

    override suspend fun getGallery(): List<PlaceEntity> {
        return localDataSource.getGallery()
    }

    override suspend fun insertAll(places: List<PlaceEntity>) {
        localDataSource.insertAll(places)
    }

    override fun findImageById(id: Int): PlaceEntity {
        return localDataSource.findImageById(id)
    }

    override suspend fun getNonDiscoveredPlaces(): List<PlaceEntity> {
        return localDataSource.getNonDiscoveredPlaces()
    }

    override suspend fun updateDiscovered(discovered: Boolean, id: Int) {
        localDataSource.updateDiscovered(discovered, id)
    }

    override suspend fun updateCaught(caught: Boolean, id: Int) {
        localDataSource.updateCaught(caught, id)
    }

    override suspend fun getRanking(): Response<List<Score>> {
        return remoteDataSource.getRanking()
    }
}