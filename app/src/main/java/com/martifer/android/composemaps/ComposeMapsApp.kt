package com.martifer.android.composemaps

import android.app.Application
import com.martifer.android.composemaps.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class ComposeMapsApp: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@ComposeMapsApp)
            modules(
                listOf(
                    repositoryModule,
                    localModule,
                    remoteModule,
                    homeViewModelModule,
                    testViewModelModule,
                    gameViewModelModule,
                    galleryViewModelModule,
                    cameraViewModelModule,
                    rankingViewModelModule,
                    dispatchersModule,
                    dtoMapperModule,
                    serviceModule
                    //useCasesModule
                )
            )
        }
    }
}