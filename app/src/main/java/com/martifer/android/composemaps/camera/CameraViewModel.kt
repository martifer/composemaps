package com.martifer.android.composemaps.camera

import android.util.Log
import androidx.lifecycle.ViewModel
import com.martifer.android.composemaps.data.RepositoryImpl
import com.martifer.android.composemaps.data.remote.model.ResNode
import kotlinx.coroutines.*
import retrofit2.Response
import java.io.InputStream

class CameraViewModel(private val repository: RepositoryImpl,
): ViewModel() {

    val TAG = "Camera-allappli"

    var job: Job? = null

    val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
       // onError("Exception: ${throwable.localizedMessage}")
        Log.v(TAG, "coroutine exeption : " + throwable.localizedMessage)
    }

    suspend fun uploadImage(inputStream: InputStream){

        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {

            Log.v(TAG, "uploadImage: " + inputStream.toString())
            val response : Response<ResNode> = repository.postImage(inputStream = inputStream)
            if(response.isSuccessful) {
                Log.v(TAG, "uploadImage: success" + response.message())
            } else {
                Log.v(TAG, "uploadImage: error" + response.message())
            }

        }
    }
}