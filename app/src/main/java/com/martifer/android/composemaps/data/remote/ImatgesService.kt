package com.martifer.android.composemaps.data.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ImatgesService {

    private val BASE_URL = ""

    fun getImatgesService(): ImatgeAPI {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ImatgeAPI::class.java)
    }
}