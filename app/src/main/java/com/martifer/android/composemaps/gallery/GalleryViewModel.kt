package com.martifer.android.composemaps.gallery

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.martifer.android.composemaps.data.PlaceEntity
import com.martifer.android.composemaps.data.RepositoryImpl
import kotlinx.coroutines.*


class GalleryViewModel(
    private val repository: RepositoryImpl,
) : ViewModel() {

    var job: Job? = null

    val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        Log.v(TAG, "fetchGallery: exception coroutine:  ${throwable.localizedMessage}")
    }

    private val TAG = "GalleryVM-allappli"

    var isLoading = mutableStateOf(true)

    var galleryList = mutableStateOf<List<PlaceEntity>>(listOf())

    init {
        getGallery()
    }

    fun getGallery() {

        /*iewModelScope.launch {
            isLoading.value = false
            galleryList.value = repository.getGallery()
        }*/

        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {

            val data = repository.getGallery()

            withContext(Dispatchers.Main) {
                isLoading.value = false
                galleryList.value = data
                Log.v(TAG, "fetchGallery: success we found: " + galleryList.value.size)
                //response.body()?.let { domainMapper.fromEntityList(it) }?.let { insertAll(it) }
            }
        }
    }
}