package com.martifer.android.composemaps.domain

data class Score(
    val name: String,
    val score: Int,
)
