package com.martifer.android.composemaps.domain

import com.martifer.android.composemaps.data.PlaceEntity
import com.martifer.android.composemaps.data.remote.model.PlaceDto
import com.martifer.android.composemaps.data.remote.model.ResNode
import retrofit2.Response
import java.io.InputStream

interface Repository {
    suspend fun getProgress(): Int
    suspend fun getCount(): Int

    suspend fun fetchImages(): Response<List<PlaceDto>>
    suspend fun postImage(inputStream: InputStream): Response<ResNode>
    suspend fun getGallery(): List<PlaceEntity>
    suspend fun insertAll(users: List<PlaceEntity>)

    fun findImageById(id: Int): PlaceEntity
    suspend fun getNonDiscoveredPlaces(): List<PlaceEntity>
    suspend fun updateDiscovered(discovered: Boolean, id: Int)
    suspend fun updateCaught(caught: Boolean, id: Int)

    suspend fun getRanking(): Response<List<Score>>
}