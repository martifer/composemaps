package com.martifer.android.composemaps.data.local

import com.martifer.android.composemaps.data.PlaceEntity

class LocalDataSource(
    private val database: PlacesDatabase
) {
    suspend fun getProgress(): Int {
        return database.placesDao().getProgress()
    }

    suspend fun getCount(): Int {
        return database.placesDao().getCount()
    }

    suspend fun insertAll(placesEntity: List<PlaceEntity>) {
        database.placesDao().insertAll(placesEntity)
    }

    fun findImageById(id: Int): PlaceEntity {
        return database.placesDao().findImageById(id)
    }

    suspend fun getNonDiscoveredPlaces(): List<PlaceEntity> {
        return database.placesDao().getNonDiscoveredPlaces()
    }

    suspend fun updateDiscovered(discovered: Boolean, id: Int) {
        return database.placesDao().updateDiscovered(discovered, id)
    }

    suspend fun updateCaught(caught: Boolean, id: Int) {
        return database.placesDao().updateCaught(caught, id)
    }

    suspend fun getGallery(): List<PlaceEntity> {
        return database.placesDao().getGallery()
    }

}