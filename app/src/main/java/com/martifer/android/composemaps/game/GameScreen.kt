package com.martifer.android.composemaps.game

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.google.android.libraries.maps.CameraUpdateFactory
import com.google.android.libraries.maps.MapView
import com.google.android.libraries.maps.model.LatLng
import com.google.maps.android.ktx.addMarker
import com.google.maps.android.ktx.awaitMap
import com.martifer.android.composemaps.Screen
import kotlinx.coroutines.*
import org.koin.androidx.compose.getViewModel


@Composable
fun GameScreen(navController: NavController, viewModel: GameViewModel) {
    Text(text = "Game", color = Color.White)
    Column(modifier = Modifier.fillMaxSize(), verticalArrangement = Arrangement.Center) {
        Spacer(Modifier.height(32.dp))
        Button(
            onClick = { navController.navigate(Screen.HomeScreen.route) },
            contentPadding = PaddingValues(
                start = 20.dp,
                top = 12.dp,
                end = 20.dp,
                bottom = 12.dp
            )

        ) {
            // Inner content including an icon and a text label
            Icon(
                Icons.Filled.Favorite,
                contentDescription = "Favorite",
                modifier = Modifier.size(ButtonDefaults.IconSize)
            )
            Spacer(Modifier.size(ButtonDefaults.IconSpacing))
            Text("Home")
        }
        Text(
            modifier = Modifier.align(Alignment.CenterHorizontally),
            text = "hola mapa",
            color = Color.White,
            style = MaterialTheme.typography.h6,
            textAlign = TextAlign.Center
        )
        Spacer(Modifier.height(16.dp))
        GameMapView("exploreModel.city.latitude", "exploreModel.city.longitude", navController)
    }


}

@Composable
private fun GameMapView(latitude: String, longitude: String, navController: NavController) {
    // The MapView lifecycle is handled by this composable. As the MapView also needs to be updated
    // with input from Compose UI, those updates are encapsulated into the MapViewContainer
    // composable. In this way, when an update to the MapView happens, this composable won't
    // recompose and the MapView won't need to be recreated.
    val mapView = rememberMapViewWithLifecycle()
    MapViewContainer(mapView, latitude, longitude, navController)
}

@Composable
private fun MapViewContainer(
    map: MapView,
    latitude: String,
    longitude: String,
    navController: NavController
) {

    val viewModel = getViewModel<GameViewModel>()
    val cameraPosition = remember(latitude, longitude) {
        val lat = 41.827258
        val lon = 1.541916
        LatLng(lat, lon)
    }

    val context = LocalContext.current

    LaunchedEffect(map, context) {
        val googleMap = map.awaitMap()
        googleMap.addMarker { position(cameraPosition) }
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(cameraPosition))
        googleMap.setOnMapClickListener {
            //navController.navigate(Screen.GalleryScreen.route)
            checkResult(context = context, navController = navController, viewModel = viewModel, it)
        }
    }


    var zoom by rememberSaveable(map) { mutableStateOf(InitialZoom) }
    ZoomControls(zoom) {
        zoom = it.coerceIn(MinZoom, MaxZoom)
    }

    val coroutineScope = rememberCoroutineScope()
    AndroidView({ map }) { mapView ->
        // Reading zoom so that AndroidView recomposes when it changes. The getMapAsync lambda
        // is stored for later, Compose doesn't recognize state reads
        val mapZoom = zoom
        coroutineScope.launch {
            val googleMap = mapView.awaitMap()
            googleMap.setZoom(mapZoom)
            // Move camera to the same place to trigger the zoom update
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(cameraPosition))


        }
    }
}

fun checkResult(
    context: Context,
    navController: NavController,
    viewModel: GameViewModel,
    latLng: LatLng
) {

    /*CoroutineScope(Dispatchers.IO).launch {
        delay(3000)
        withContext(Dispatchers.Main) {
            showToast(context = context )
            navController.navigate(Screen.GalleryScreen.route)
        }
    }*/



    CoroutineScope(Dispatchers.Main).launch {
        showToast(context = context)
        withContext(Dispatchers.IO) {
            viewModel.checkResult(latLng)
        }
        delay(3000)
        viewModel.imageScreen.value = true
        viewModel.imageIndex.value++
        //navController.navigate(Screen.GalleryScreen.route)
        ///withContext(Dispatchers.IO) {

        //}
    }
}

/*fun showDialog(encert: Boolean) {
    var missatge: String
    if(encert) {
        missatge = "Correcte"
    } else {
        missatge = "Error"
    }

    val builder = AlertDialog.Builder(Activity.)
    builder.setTitle("Resultat")
    builder.setMessage(missatge)
}*/


fun showToast(context: Context) {
    val text = "Hello toast!"
    val duration = Toast.LENGTH_LONG
    val toast = Toast.makeText(context, text, duration)
    toast.show()
}

private const val InitialZoom = 8f
const val MinZoom = 2f
const val MaxZoom = 20f

@Composable
private fun ZoomControls(
    zoom: Float,
    onZoomChanged: (Float) -> Unit
) {
    Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
        ZoomButton("-", onClick = { onZoomChanged(zoom * 0.8f) })
        ZoomButton("+", onClick = { onZoomChanged(zoom * 1.2f) })
    }
}

@Composable
private fun ZoomButton(text: String, onClick: () -> Unit) {
    Button(
        modifier = Modifier.padding(8.dp),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = MaterialTheme.colors.onPrimary,
            contentColor = MaterialTheme.colors.primary
        ),
        onClick = onClick
    ) {
        Text(text = text, style = MaterialTheme.typography.h5)
    }
}


@Preview
@Composable
fun PreviewGameScreen() {
    val navController = rememberNavController()
    //GameScreen(navController)
}