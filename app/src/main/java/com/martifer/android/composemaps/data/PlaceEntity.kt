package com.martifer.android.composemaps.data

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.martifer.android.composemaps.domain.Place

@Entity(tableName = "place_table")
class PlaceEntity(
    @Embedded
    var place: Place
) {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

}

fun PlaceEntity.toPlace(): Place {
    return Place(
        name = place.name,
        //id = place.id,
        source = place.source,
        latitude = place.latitude,
        longitude = place.longitude,
        radius = place.radius,
        discovered = place.discovered,
        hidden = place.hidden,
        caught = place.caught
    )
}