package com.martifer.android.composemaps.data.remote

import com.martifer.android.composemaps.data.remote.model.PlaceDto
import com.martifer.android.composemaps.data.remote.model.ResNode
import com.martifer.android.composemaps.domain.Score
import kotlinx.coroutines.delay
import retrofit2.Response
import java.io.InputStream

class FakeRemoteDataSource {

    val serviceDelay = 1000L

    suspend fun getImages(): Response<List<PlaceDto>> {
        delay(serviceDelay)
        val body = listOf(
            PlaceDto("nom", "source", 2.33, 3.444, 5.0)
        )
        return Response.success(body)
    }

    suspend fun postImage(inputStream: InputStream): Response<ResNode> {
        delay(serviceDelay)
        val body = ResNode("status", "msg")

        return Response.success(body)
    }

    suspend fun getRanking(): Response<List<Score>> {
        delay(serviceDelay)
        val body = listOf(
            Score("Martí", 32),
            Score("Silvia", 33),
            Score("Anna", 24),
            Score("Albert", 19),
        )
        return Response.success(body)
    }
}