package com.martifer.android.composemaps.data.remote

import com.martifer.android.composemaps.data.remote.model.PlaceDto
import com.martifer.android.composemaps.data.remote.model.ResNode
import com.martifer.android.composemaps.domain.Score
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface ImatgeAPI {

    @GET
    suspend fun getImages(@Url url :String): Response<List<PlaceDto>>

    @POST
    suspend fun postImage(
        @Url url :String,
        @Body body: RequestBody
    ): Response<String>


    @Multipart
    @POST
    suspend fun postImageMulti(
        @Url url :String,
        @Part part: MultipartBody.Part
    ): Response<ResNode>

    @POST
    suspend fun postImageBinary(
        @Url url :String,
        @Body body: RequestBody
    ): Response<ResNode>

    @GET
    suspend fun getRanking(@Url url :String): Response<List<Score>>



}