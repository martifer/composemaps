package com.martifer.android.composemaps.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.martifer.android.composemaps.data.PlaceEntity

@Database(entities = [PlaceEntity::class], version = 1)
abstract class PlacesDatabase : RoomDatabase() {

    abstract fun placesDao(): PlacesDao
}