package com.martifer.android.composemaps.permission

import android.Manifest
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.currentCompositionLocalContext
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.rememberCompositionContext
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import com.google.accompanist.permissions.rememberPermissionState

@ExperimentalPermissionsApi
@Composable
fun LocationPermisionUI(launchBackGroundService: () -> Unit) {

    //val cameraPermissionState = rememberPermissionState(android.Manifest.permission.CAMERA)
    val permissionsState = rememberMultiplePermissionsState(
        permissions = listOf(
            //Manifest.permission.ACCESS_BACKGROUND_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
        )
    )

    val context = LocalContext.current

    permissionsState.permissions.forEach { perm ->
        when(perm.permission) {
            Manifest.permission.ACCESS_FINE_LOCATION -> {
                when {
                    perm.hasPermission -> {
                        Text(text = "Location permission accepted")
                    }
                    perm.shouldShowRationale -> {
                        Text(text = "Location permission is needed" +
                                "to access the ubication")
                        PermissionUtils.shouldShowPermissionRationale(
                            context,
                            perm.permission
                        )

                    }
                    /*perm.isPermanentlyDenied() -> {
                        Text(text = "Camera permission was permanently" +
                                "denied. You can enable it in the app" +
                                "settings.")
                    }*/
                }
            }
            Manifest.permission.ACCESS_COARSE_LOCATION -> {
                when {
                    perm.hasPermission -> {
                        Text(text = "Location Coarse permission accepted")
                    }
                    perm.shouldShowRationale -> {
                        Text(text = "Record audio permission is needed" +
                                "to access the camera")
                        PermissionUtils.shouldShowPermissionRationale(
                            context,
                            perm.permission
                        )
                    }
                }
            }

            Manifest.permission.ACCESS_BACKGROUND_LOCATION -> {
                when {
                    perm.hasPermission -> {
                        Text(text = "Location Background permission accepted")
                    }
                    perm.shouldShowRationale -> {
                        Text(text = "Location Background permission is needed" )
                        PermissionUtils.shouldShowPermissionRationale(
                            context,
                            perm.permission
                        )
                    }
                }
            }

        }
        if(permissionsState.allPermissionsGranted){
            launchBackGroundService()
        }
    }
}