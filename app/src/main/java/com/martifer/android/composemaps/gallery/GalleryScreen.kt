package com.martifer.android.composemaps.gallery

import android.util.Log
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import coil.compose.rememberImagePainter
import coil.transform.CircleCropTransformation
import com.martifer.android.composemaps.Screen
import com.martifer.android.composemaps.data.PlaceEntity
import org.koin.androidx.compose.getViewModel
import kotlin.random.Random

@ExperimentalFoundationApi
@Composable
fun GalleryScreen(navController: NavController) {

    val viewModel = getViewModel<GalleryViewModel>()
    val galleryList by remember { viewModel.galleryList }
    val isLoading by remember { viewModel.isLoading }

    Log.v("GalleryScreen", "inici:" + isLoading)

    Text(text = "Gallery", color = Color.White)
    Button(
        onClick = { navController.navigate(Screen.HomeScreen.route) },
        contentPadding = PaddingValues(
            start = 20.dp,
            top = 12.dp,
            end = 20.dp,
            bottom = 12.dp
        )

    ) {
        // Inner content including an icon and a text label
        Icon(
            Icons.Filled.Favorite,
            contentDescription = "Favorite",
            modifier = Modifier.size(ButtonDefaults.IconSize)
        )
        Spacer(Modifier.size(ButtonDefaults.IconSpacing))
        Text("Home")

    }
    if (!isLoading) {
        Text(text = "ImageGrid")
        ImageGrid(galleryList)

    } else {
        Text(text = "Loading")
    }
}

@ExperimentalFoundationApi
@Composable
fun ImageGrid(images: List<PlaceEntity>) {

    val check = "https://bitbucket.org/martifer/imatges/raw/master/check.png"

    LazyVerticalGrid(
        cells = GridCells.Fixed(3),
        contentPadding = PaddingValues(8.dp)
    ) {

        items(images.size) { item ->

            Card(
                modifier = Modifier.padding(4.dp),
                backgroundColor = Color(
                    red = if (images[item].place.caught) 0 else 255,
                    green = if (images[item].place.caught) 255 else 0,
                    blue = 0
                    /*red = Random.nextInt(0, 255),
                    green = Random.nextInt(0, 255),
                    blue = Random.nextInt(0, 255)*/
                )
            ) {
                /*Text(
                    text = "patata",
                    fontSize = 42.sp,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.padding(24.dp)
                )*/
                // basic
                /*Image(
                    painter = rememberImagePainter(source),
                    contentDescription = null,
                    modifier = Modifier.size(128.dp)
                )*/
                // advanced
                Image(
                    painter = rememberImagePainter(
                        data = images[item].place.source, //if (images[item].place.discovered) images[item].place.source else check,
                        builder = {
                            transformations(CircleCropTransformation())
                        }
                    ),
                    contentDescription = null,
                    modifier = Modifier.size(128.dp)
                )
            }
        }
    }
}


@ExperimentalFoundationApi
@Composable
fun IconGrid() {

    val data = listOf("☕️", "🙂", "🥛", "🎉", "📐", "🎯", "🧩", "😄", "🥑")
    //val data = listOf("Item 1", "Item 2", "Item 3", "Item 4", "Item 5")
    LazyVerticalGrid(
        cells = GridCells.Fixed(3),
        contentPadding = PaddingValues(8.dp)
    ) {
        items(data.size) { item ->
            Card(
                modifier = Modifier.padding(4.dp),
                backgroundColor = Color(
                    red = Random.nextInt(0, 255),
                    green = Random.nextInt(0, 255),
                    blue = Random.nextInt(0, 255)
                )
            ) {
                Text(
                    text = data[item],
                    fontSize = 42.sp,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.padding(24.dp)
                )
            }
        }
    }
}

@ExperimentalFoundationApi
@Preview
@Composable
fun PreviewGalleryScreen() {
    val navController = rememberNavController()
    GalleryScreen(navController)
}