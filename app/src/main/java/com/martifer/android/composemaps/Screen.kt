package com.martifer.android.composemaps

sealed class Screen(val route: String) {
    object HomeScreen: Screen("home_screen")
    object GameScreen: Screen("game_screen")
    object GalleryScreen: Screen("gallery_screen")
    object CameraScreen: Screen("camera_screen")
    object ImageScreen: Screen("image_screen")
    object GameMainScreen: Screen("game_main_screen")
    object RankingScreen: Screen("ranking_screen")
}
