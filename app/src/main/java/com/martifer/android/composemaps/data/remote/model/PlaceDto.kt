package com.martifer.android.composemaps.data.remote.model

import com.google.gson.annotations.SerializedName

data class PlaceDto(
    @SerializedName("nom")
    val name: String,

    @SerializedName("recurs")
    val source: String,

    @SerializedName("longitud")
    val longitude: Double,

    @SerializedName("latitud")
    val latitude: Double,

    @SerializedName("radi")
    val radius: Double

)