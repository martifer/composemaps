package com.martifer.android.composemaps

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.martifer.android.composemaps.camera.CameraScreen
import com.martifer.android.composemaps.gallery.GalleryScreen
import com.martifer.android.composemaps.game.GameMainScreen
import com.martifer.android.composemaps.home.HomeScreen
import com.martifer.android.composemaps.ranking.RankingScreen

@ExperimentalFoundationApi
@Composable
fun Navigation(activityFunWithParam: () -> Unit) {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = Screen.HomeScreen.route) {
        composable(Screen.HomeScreen.route) {
            HomeScreen(navController, activityFunWithParam)
        }
        /*composable(Screen.GameScreen.route){
            GameScreen(navController)
        }*/
        composable(Screen.GalleryScreen.route) {
            GalleryScreen(navController)
        }
        composable(Screen.CameraScreen.route) {
            CameraScreen(navController)
        }

        /*composable(Screen.ImageScreen.route){
            ImageScreen(navController)
        }*/
        composable(Screen.GameMainScreen.route) {
            GameMainScreen(navController)
        }

        composable(Screen.RankingScreen.route) {
            RankingScreen(navController)
        }

    }
}

