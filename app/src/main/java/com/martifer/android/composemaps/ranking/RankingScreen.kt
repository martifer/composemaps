package com.martifer.android.composemaps.ranking

import androidx.compose.ui.graphics.Color
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.martifer.android.composemaps.domain.Score
import org.koin.androidx.compose.getViewModel

@Composable
fun RankingScreen(navController: NavController) {

    val viewModel = getViewModel<RankingViewModel>()

    var colorNamesList = listOf("Red", "Green", "Blue", "Indigo")

    Column (
        Modifier
            .background(Color.Yellow)
            .fillMaxSize()
            .padding(8.dp)) {
        Button(onClick = { colorNamesList = colorNamesList.shuffled() }) {
            Text("Shuffle")
        }
        Spacer(Modifier.size(ButtonDefaults.IconSpacing))

        ScoreList(messages = viewModel.scoreList.value!!)
        
        LazyColumn {
            itemsIndexed(colorNamesList) { index, item ->
                Text("$index = $item")
            }
        }
    }
}

@Composable
fun ScoreList(messages: List<Score>) {
    // Remember our own LazyListState
    val listState = rememberLazyListState()

    // Provide it to LazyColumn
    LazyColumn(state = listState) {
        // ...
    }
}

@Preview
@Composable
fun PreviewRankingScreen() {
    val navController = rememberNavController()
    RankingScreen(navController)
}