package com.martifer.android.composemaps.ranking

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.martifer.android.composemaps.data.RepositoryImpl
import com.martifer.android.composemaps.data.remote.model.PlaceDtoMapper
import com.martifer.android.composemaps.domain.Score
import kotlinx.coroutines.*
import retrofit2.Response

class RankingViewModel (private val repository: RepositoryImpl,
                        private val dispatcher: Dispatchers,
                        private val domainMapper: PlaceDtoMapper
) : ViewModel() {

    var job: Job? = null
    private val TAG = "RankingVM-allappli"

    val scoreList = MutableLiveData<List<Score>>()

    var loadError = mutableStateOf("")
    var isLoading = mutableStateOf(true)

    val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        //onError("Exception: ${throwable.localizedMessage}")
    }

    init {
        getRanking()
    }

    fun getRanking() {
        job = CoroutineScope(dispatcher.IO + exceptionHandler).launch {
            val response : Response<List<Score>> = repository.getRanking()
            withContext(Dispatchers.Main) {
                if(response.isSuccessful) {
                    Log.v(TAG, "getRenking: success")
                    scoreList.value = response.body()
                    isLoading.value = false
                } else {
                    Log.v(TAG, "getRenking: error")
                    onError("Error:  ${response.message()}")

                    isLoading.value = false
                }
            }
        }
    }

    private fun onError(message: String) {
        //loadError.value = message
        isLoading.value = false
    }

}