package com.martifer.android.composemaps.home

import android.Manifest
import android.content.ContentValues.TAG
import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.delay
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.material.*
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.PointMode
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.martifer.android.composemaps.MainActivity
import com.martifer.android.composemaps.R
import com.martifer.android.composemaps.Screen
import com.martifer.android.composemaps.permission.PermissionAction
import com.martifer.android.composemaps.permission.PermissionUI
import kotlinx.coroutines.launch
import org.koin.androidx.compose.getViewModel
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin


@Composable
fun HomeScreen(navController: NavController, activityFunWithParam: () -> Unit) {

    val viewModel = getViewModel<HomeViewModel>()
    val endReached by remember { viewModel.endReached }
    val loadError by remember { viewModel.loadError }
    val isLoading by remember { viewModel.isLoading }

    val permissionCamera by remember { viewModel.permissionCamera}

    Log.v("allappli", "inici:" + isLoading)

    Column {
        LoginBox(){
            activityFunWithParam()
        }
        ButtonsFirstRow(navController = navController)
        ButtonsSecondRow(navController = navController) {
            viewModel.permissionCamera.value = true
        }

        Row () {
            Log.v("HomeScreen", "isLoading:" + isLoading)
            if(!isLoading){
                Timer(
                    totalTime = 100L* 1000L,
                    handleColor = Color.Green,
                    inactiveBarColor = Color.DarkGray,
                    activeBarColor = Color(0xFF37B900),
                    modifier = Modifier.size(200.dp)
                )
            } else {
                Text(text = "Loading")
                CircularProgressIndicator(
                    modifier = Modifier.size(300.dp)
                )
            }
        }

        val scaffoldState = rememberScaffoldState()
        val scope = rememberCoroutineScope()
        val context = LocalContext.current

        if(permissionCamera){

            PermissionUI(
                context,
                Manifest.permission.CAMERA,
                stringResource(id = R.string.permission_location_rationale),
                scaffoldState
            ) { permissionAction ->
                when (permissionAction) {
                    is PermissionAction.OnPermissionGranted -> {
                        //permissionTestViewModel.setPerformLocationAction(false)
                        //Todo: do something now as we have camera permission

                        viewModel.permissionCamera.value = false

                        navController.navigate(Screen.CameraScreen.route)

                        Log.d(TAG, "Location has been granted")
                        scope.launch {
                            scaffoldState.snackbarHostState.showSnackbar("Location permission granted!")
                        }
                    }
                    is PermissionAction.OnPermissionDenied -> {
                        //permissionTestViewModel.setPerformLocationAction(false)
                        viewModel.permissionCamera.value = false
                    }
                }
            }

        }


    }
}



@Composable
fun Timer(
    totalTime: Long,
    handleColor: Color,
    inactiveBarColor: Color,
    activeBarColor: Color,
    modifier: Modifier,
    initialValue: Float = 0f,
    strokeWidth: Dp = 5.dp
    ) {

    var size by remember {
        mutableStateOf(IntSize.Zero)
    }
    var value by remember {
        mutableStateOf(initialValue)
    }
    var currentTime by remember {
        mutableStateOf(totalTime)
    }
    var isTimerRunning by remember {
        mutableStateOf(false)
    }

    val viewModel = getViewModel<HomeViewModel>()

    /*LaunchedEffect(key1 = currentTime, key2 = isTimerRunning) {
        if(currentTime > 0 && isTimerRunning) {
            delay(100L)
            currentTime -= 100L
            value = currentTime / totalTime.toFloat()
        }
    }*/


    val progress = viewModel.progress
    val count = viewModel.count

    val valorDesitjat = progress.toFloat()/count.toFloat()//.75f
    val amplitut = valorDesitjat
    var tempsTotal by remember {
        mutableStateOf((11f/2f)*Math.PI)
    }

    var temps by remember {
        mutableStateOf(tempsTotal)
    }

    // comentar la linia d'abaix si es vol testejar amb botó
    isTimerRunning = true

    LaunchedEffect(key1 = temps, key2 = isTimerRunning) {

        if(temps > 0 && isTimerRunning) {
            val factor = temps/ tempsTotal
            //currentTime -= 100L
            delay(10L)
            temps -= .2
            value = valorDesitjat + Math.sin(temps).toFloat() * amplitut*factor.toFloat()    //currentTime / totalTime.toFloat()
        }
    }


    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
            .onSizeChanged {
                size = it
            }
    ) {
        Canvas(modifier = modifier) {
            drawArc(
                color = inactiveBarColor,
                startAngle = -215f,
                sweepAngle = 250f,
                useCenter = false,
                size = Size(size.width.toFloat(), size.height.toFloat()),
                style = Stroke(strokeWidth.toPx(), cap = StrokeCap.Round)
            )
            drawArc(
                color = activeBarColor,
                startAngle = -215f,
                sweepAngle = 250f * value,
                useCenter = false,
                size = Size(size.width.toFloat(), size.height.toFloat()),
                style = Stroke(strokeWidth.toPx(), cap = StrokeCap.Round)
            )
            val center = Offset(size.width / 2f, size.height / 2f)
            val beta = (250f * value + 145f) * (PI / 180f).toFloat()
            val r = size.width / 2f
            val a = cos(beta) * r
            val b = sin(beta) * r
            drawPoints(
                listOf(Offset(center.x + a, center.y + b)),
                pointMode = PointMode.Points,
                color = handleColor,
                strokeWidth = (strokeWidth * 3f).toPx(),
                cap = StrokeCap.Round
            )
        }
        Text(
            text = (currentTime / 1000L).toString(),
            fontSize = 44.sp,
            fontWeight = FontWeight.Bold,
            color = Color.White
        )
        /*Button(   /// descomentar button per test
            onClick = {
                if(temps <= 0L) {
                    //currentTime = totalTime
                    temps = tempsTotal
                    isTimerRunning = true
                } else {
                    isTimerRunning = !isTimerRunning
                }
            },
            modifier = Modifier.align(Alignment.BottomCenter),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = if (!isTimerRunning || currentTime <= 0L) {
                    Color.Green
                } else {
                    Color.Red
                }
            )
        ) {
            Text(
                text = if (isTimerRunning && currentTime >= 0L) "Stop"
                else if (!isTimerRunning && currentTime >= 0L) "Start"
                else "Restart"
            )
        }*/
    }

}

@Preview
@Composable
fun PreviewHomeScreen() {
    val navController = rememberNavController()
    HomeScreen(navController,{})
}