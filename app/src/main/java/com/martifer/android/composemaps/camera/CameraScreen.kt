package com.martifer.android.composemaps.camera

import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavController
import kotlinx.coroutines.launch
import org.koin.androidx.compose.getViewModel
import java.io.InputStream

@Composable
fun CameraScreen(navController: NavController) {

    val TAG = "Camera-allappli"

    val viewModel = getViewModel<CameraViewModel>()
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()
    CameraView(onImageCaptured = { uri, fromGallery ->
        Log.d(TAG, "Image Uri Captured from Camera View")
//Todo : use the uri as needed
        val imageStream: InputStream? = context.contentResolver.openInputStream(uri)
        coroutineScope.launch {
            viewModel.uploadImage(imageStream!!)
        }
    }, onError = { imageCaptureException ->

        coroutineScope.launch {
            //scaffoldState.snackbarHostState.showSnackbar("An error occurred while trying to take a picture")
        }
    })


}