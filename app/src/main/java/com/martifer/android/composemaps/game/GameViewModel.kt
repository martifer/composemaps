package com.martifer.android.composemaps.game

import android.location.Location
import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.google.android.libraries.maps.model.LatLng
import com.martifer.android.composemaps.data.PlaceEntity
import com.martifer.android.composemaps.data.RepositoryImpl
import com.martifer.android.composemaps.data.remote.model.PlaceDtoMapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class GameViewModel(
    private val repository: RepositoryImpl,
    private val domainMapper: PlaceDtoMapper
) : ViewModel() {

    private val TAG = "GameVM-allappli"

    var imageScreen = mutableStateOf(true)
    var isGameReady = mutableStateOf(false)
    var imageIndex = mutableStateOf(0)

    lateinit var listPlaces: List<PlaceEntity>

    fun checkResult(latLng: LatLng) {
        Log.v(TAG, "checkResult")
        val place = listPlaces.get(imageIndex.value)//repository.findImageById(imageIndex.value)


        val results = FloatArray(1)
        Location.distanceBetween(
            place.place.latitude,
            place.place.longitude,
            latLng.latitude,
            latLng.longitude,
            results
        )

        Log.v(TAG, "checkResult: Distance: " + results[0])
        if (results[0] < 10000) { // TODO: modificar el radi captura
            CoroutineScope(Dispatchers.IO).launch {
                repository.updateDiscovered(true, place.id)
            }
        }

    }

    init {
        prepareGame()
    }

    fun prepareGame() {
        CoroutineScope(Dispatchers.IO).launch {
            val placesList = repository.getNonDiscoveredPlaces()
            Log.v(TAG, "prepareGame, places found:" + placesList.size)
            listPlaces = placesList.shuffled()
            isGameReady.value = true
        }
    }
}