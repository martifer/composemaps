package com.martifer.android.composemaps.domain

import coil.fetch.SourceResult

data class Place(
    val name: String,
    val source: String,
    val latitude: Double,
    val longitude: Double,
    val radius: Double,
    val discovered: Boolean,
    val caught: Boolean,
    val hidden: Boolean
)
