package com.martifer.android.composemaps.home

import androidx.lifecycle.ViewModel
import com.martifer.android.composemaps.data.RepositoryImpl
import com.martifer.android.composemaps.data.remote.model.PlaceDtoMapper
import kotlinx.coroutines.Dispatchers

class TestViewModel(
    private val repository: RepositoryImpl,
    private val dispatcher: Dispatchers,
    private val domainMapper: PlaceDtoMapper
) : ViewModel()