package com.martifer.android.composemaps.data.remote

import com.martifer.android.composemaps.data.remote.model.PlaceDto
import com.martifer.android.composemaps.data.remote.model.ResNode
import com.martifer.android.composemaps.domain.Score
import com.martifer.android.composemaps.other.Constants
import kotlinx.coroutines.delay
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit2.Response
import java.io.InputStream


class RemoteDataSource(
    private val imatgeAPI: ImatgeAPI
) {


    suspend fun getImages(): Response<List<PlaceDto>> {
        val path = "raw/master/imatges.json"
        return imatgeAPI.getImages(Constants.IMAGES_URI + path)
    }

    //https://stackoverflow.com/questions/39953457/how-to-upload-an-image-file-in-retrofit-2
    suspend fun postImage(inputStream: InputStream): Response<ResNode> {

        val body = RequestBody.create("application/octet-stream".toMediaTypeOrNull(), inputStream.readBytes()) // original
        val path = "uploadstream"

        // below old multipard method
        /*val part = MultipartBody.Part.createFormData(
            "pic", "myPic", body
        )
        //return imatgeAPI.postImageMulti(Constants.BACKEND_URI + path,part)*/
        return imatgeAPI.postImageBinary(Constants.BACKEND_URI + path, body)
    }

    suspend fun getRanking(): Response<List<Score>> {
        val path = "getscore"
        delay(1000L)
        val body = listOf(
            Score("Martí", 32),
            Score("Silvia", 33),
            Score("Anna", 24),
            Score("Albert", 19),
        )
        return Response.success(body)



        //return imatgeAPI.getRanking(Constants.BACKEND_URI + path)
    }
}