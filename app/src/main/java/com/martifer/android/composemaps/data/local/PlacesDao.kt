package com.martifer.android.composemaps.data.local

import androidx.room.*
import com.martifer.android.composemaps.data.PlaceEntity
import kotlinx.coroutines.flow.Flow
@Dao
interface PlacesDao {
    //@Query("SELECT COUNT() FROM place_table WHERE discovered = 1")
    @Query("SELECT COUNT(*) FROM place_table WHERE caught IS 1")
    suspend fun getProgress(): Int

    @Query("SELECT COUNT() FROM place_table" )
    suspend fun getCount(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(placesEntity: List<PlaceEntity>)

    @Query("SELECT * FROM place_table ORDER BY id ASC")
    fun readPlaces(): Flow<List<PlaceEntity>>

    @Query("SELECT * FROM place_table WHERE discovered = 0 ORDER BY id ASC")
    //@Query("SELECT * FROM place_table ORDER BY id ASC")
    fun getGallery(): List<PlaceEntity>

    @Query("SELECT * FROM place_table WHERE discovered = 0 ORDER BY id ASC")
    suspend fun getNonDiscoveredPlaces(): List<PlaceEntity>

    @Query("SELECT * FROM place_table WHERE id = :id ORDER BY id ASC ")
    fun findImageById(id: Int): PlaceEntity

    @Query("UPDATE place_table SET discovered=:discovered WHERE id = :id")
    suspend fun updateDiscovered(discovered: Boolean, id: Int)

    @Query("UPDATE place_table SET caught=:caught WHERE id = :id")
    suspend fun updateCaught(caught: Boolean, id: Int)

}